#!/bin/bash
CUR_PATH=$(dirname  $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

check_dependencies "$OTF2_PRINT_PATH"  || exit 1
[ -n "$MPI_MODULE_NAME" ] || MPI_MODULE_NAME=mpi
check_module "$MPI_MODULE_NAME"  || exit 1

# Building tests
check_compilation || exit 1

nfailed=0
# Running test scripts
for test in $CUR_PATH/test_*.sh; do
    echo "running $test"
    run_test "$test" || ((nfailed++))
done

exit $nfailed
