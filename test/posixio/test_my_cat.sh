#!/bin/bash
CUR_PATH=$(dirname  $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="my_cat"
input_file="input_file.tmp"
output_file="output_file.tmp"
size=1000
dd if=/dev/random of="$input_file" count=1000 bs=1

run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "posixio" "./$name" "${input_file}" "$output_file"

trace_filename="${name}_trace/eztrace_log.otf2"
trace_check_integrity "$trace_filename"
trace_check_enter_leave_parity  "$trace_filename"


nevent=$(echo "$size * 2 +1"|bc)
trace_check_event_type "$trace_filename" "IO_OPERATION_BEGIN" $nevent
trace_check_event_type "$trace_filename" "IO_OPERATION_COMPLETE" $nevent

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
