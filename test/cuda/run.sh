#!/bin/bash
CUR_PATH=$(dirname  $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

check_dependencies "$OTF2_PRINT_PATH" || exit 1
check_module "cuda" || exit 1

# Building tests
check_compilation || exit 1

nfailed=0
# Running test scripts
for test in $CUR_PATH/test_*.sh; do
    run_test "$test" || ((nfailed++))
done

exit $nfailed
