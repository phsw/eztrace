#!/bin/bash
CUR_PATH=$(dirname $(realpath $0))
source "$CUR_PATH/../test_utils/test_utils.sh"

name="vectorAdd"
[ -n "$EZTRACE_PATH" ] || EZTRACE_PATH=eztrace
run_and_check_command "$EZTRACE_PATH" $EZTRACE_TEST_OPTION -t "cuda" "./vectorAdd"

trace_filename="${name}_trace/eztrace_log.otf2"
trace_check_integrity "$trace_filename"
trace_check_enter_leave_parity  "$trace_filename"

trace_check_nb_enter "$trace_filename" "cuda::runtime::cudaMalloc" 3
trace_check_nb_leave "$trace_filename" "cuda::runtime::cudaMalloc" 3

trace_check_nb_enter "$trace_filename" "cuda::runtime::cudaFree" 3
trace_check_nb_leave "$trace_filename" "cuda::runtime::cudaFree" 3

trace_check_nb_enter "$trace_filename" "cuda::runtime::cudaLaunchKernel" 1
trace_check_nb_leave "$trace_filename" "cuda::runtime::cudaLaunchKernel" 1

echo PASS: $nb_pass, FAILED:$nb_failed, TOTAL: $nb_test

exit $nb_failed
