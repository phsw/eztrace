/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

/* 
 * Simple program that calls the libexample library
 */

#include <stdlib.h>
#include <stdio.h>
#include "test_pptrace.h"

#define SIZE 1024

int main(int argc, char* argv) {
  uint32_t *int_array = malloc(sizeof(uint32_t) * SIZE);
  uint32_t i, sum;

  /* initialize the arrays */
  for (i = 0; i < SIZE; i++) {
    int_array[i] = (i * i) % 31;
  }

  sum = test_pptrace_foo(int_array, SIZE);

  printf("sum int = %d\n", sum);

  return 0;
}
