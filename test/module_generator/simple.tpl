BEGIN_MODULE
NAME simple_lib
DESC "module for the simple example library"
LANGUAGE C

BEGIN_INCLUDE
#include <stdio.h>
END_INCLUDE

void appli_function1(double* array, int size)
BEGIN
  RECORD_STATE("running appli_function1")
END

double example_function1(double* array, int array_size)

int example_function2(int* array, int array_size)
BEGIN
  RECORD_STATE("running example_function2")
END

int example_fcall(   int* array,
        int array_size )
BEGIN
 CALL_FUNC
END

int example_pushpop (int* array,int array_size)
BEGIN
 PUSH_STATE("doing function example_push")
 CALL_FUNC
 POP_STATE("doing function example_push")
END


END_MODULE
