set(CMAKE_C_COMPILER ${MPICC})

add_library(eztrace-netcdf SHARED
  netcdf.c
)

target_link_libraries(eztrace-netcdf
PUBLIC
    eztrace-core
    eztrace-lib
    eztrace-instrumentation
)

target_include_directories(eztrace-netcdf
  PUBLIC
    ${netcdf_INCLUDE_DIRS}
  PRIVATE
    ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
)

#---------------------------------------------

install(TARGETS eztrace-netcdf
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
