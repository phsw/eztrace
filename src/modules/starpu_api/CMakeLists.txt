
# Create : libeztrace-starpuv2.so
add_library(eztrace-starpuv2 SHARED
  starpu.c
)

target_include_directories(eztrace-starpuv2
  PRIVATE
    ${STARPU_INCLUDE_DIRS}
)

target_link_libraries(eztrace-starpuv2
  PRIVATE
    dl
    eztrace-core
    eztrace-lib
    ${STARPU_LIBRARIES}
)

install(TARGETS eztrace-starpuv2
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
