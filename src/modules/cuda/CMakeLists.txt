if(NOT DEFINED CMAKE_CUDA_ARCHITECTURES)
  set(CMAKE_CUDA_ARCHITECTURES 52 60 61 75 CACHE STRING "CUDA architectures" FORCE)
endif()
if(NOT DEFINED CMAKE_CUDA_CREATE_SHARED_LIBRARY)
  set(CMAKE_CUDA_CREATE_SHARED_LIBRARY ON)
endif()

enable_language(CXX)
#enable_language(CUDA)


find_package(CUDA REQUIRED)

#set(CUDA_SEPARABLE_COMPILATION ON)
#set(CUDA_PROPAGATE_HOST_FLAGS ON)

#link_directories(${CUDA_TOOLKIT_ROOT_DIR}/lib)
#link_directories(${CUDA_TOOLKIT_ROOT_DIR}/lib64)

add_library(eztrace-cuda SHARED)

target_sources(eztrace-cuda PRIVATE
  cuda.c
  cuda_runtime.c
  cuda_driver.c
  )

target_link_libraries(eztrace-cuda
  eztrace-core
  eztrace-lib
  ${CUDA_LIBRARIES}
  ${CUDA_cupti_LIBRARY}
  )

target_include_directories(eztrace-cuda
  PRIVATE
  ${CUDA_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}
  ${CMAKE_SOURCE_DIR}/src/core/include/eztrace-core/
  ${CUDAToolkit_CUPTI_INCLUDE_DIR}
  )

#set_target_properties(eztrace-cuda PROPERTIES
#  LINKER_LANGUAGE CUDA
#  LANGUAGE CUDA
#  )

#message(STATUS "CUDA_SDK_ROOT_DIR=${CUDA_SDK_ROOT_DIR}")
#message(STATUS "CUDA_TOOLKIT_ROOT_DIR=${CUDA_TOOLKIT_ROOT_DIR}")
#message(STATUS "CUDA_LIBRARIES=${CUDA_LIBRARIES}")


#--------------------------------------------

install(TARGETS eztrace-cuda
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
  INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)
