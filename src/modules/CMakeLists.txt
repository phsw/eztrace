# Default modules
if(EZTRACE_ENABLE_MEMORY)
  add_subdirectory(memory)
endif()
if(EZTRACE_ENABLE_POSIXIO)
  add_subdirectory(posixio)
endif()
if(EZTRACE_ENABLE_PTHREAD)
  add_subdirectory(pthread)
endif()

# Optional modules
if (EZTRACE_ENABLE_MPI)
  add_subdirectory(mpi)
endif ()

if (EZTRACE_ENABLE_OPENMP)
  add_subdirectory(omp)
endif()

if (EZTRACE_ENABLE_CUDA)
  add_subdirectory(cuda)
endif ()

if (EZTRACE_ENABLE_STARPU)
  add_subdirectory(starpu)
  add_subdirectory(starpu_api)
endif ()

if (EZTRACE_ENABLE_OMPT)
  add_subdirectory(ompt)
endif()

if(EZTRACE_ENABLE_IOTRACER)
  add_subdirectory(iotracer)
endif()

if(EZTRACE_ENABLE_PNETCDF)
  add_subdirectory(pnetcdf)
endif()

if(EZTRACE_ENABLE_NETCDF)
  add_subdirectory(netcdf)
endif()

if(EZTRACE_ENABLE_COMPILER_INSTRUMENTATION)
  add_subdirectory(compiler_instrumentation)
endif()

# Some modules have not been ported to OTF2 yet

#if (EZTRACE_ENABLE_PAPI)
#  add_subdirectory(papi)
#endif ()

