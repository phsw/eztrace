FROM debian:bookworm

# Install build tools
RUN apt update \
  && apt install -y build-essential cmake wget git gawk

# Install OTF2 in /opt/otf2 and make otf-print available
RUN wget http://perftools.pages.jsc.fz-juelich.de/cicd/otf2/tags/otf2-2.3/otf2-2.3.tar.gz -P /tmp \
&& (cd /tmp  && tar xzf /tmp/otf2-2.3.tar.gz) \
&& (cd /tmp/otf2-2.3 && ./configure  --prefix=/usr/local&& make  &&make install) \
&& rm -rf /tmp/otf2-2.3


# Install Opari2 in /usr/local
RUN wget http://perftools.pages.jsc.fz-juelich.de/cicd/opari2/tags/opari2-2.0.6/opari2-2.0.6.tar.gz -P /tmp \
&& (cd /tmp && tar xzf /tmp/opari2-2.0.6.tar.gz) \
&& (cd /tmp/opari2-2.0.6 && ./configure --prefix=/usr/local && make  &&make install) \
&& rm -rf /tmp/opari2-2.0.6

# Install OpenMPI
RUN apt install -y openmpi-bin openmpi-common libopenmpi-dev

# Install Clang
RUN apt install -y clang libomp-dev

# Install StarPU
RUN apt install -y libstarpu-dev pkgconf

# Install EZTrace
RUN git clone https://gitlab.com/eztrace/eztrace.git \
&& ( mkdir eztrace/build && cd eztrace/build &&  \
   cmake .. \
   -DENABLE_BIN_INSTRUMENTATION=OFF\
   -DOTF2_PATH=/opt/otf2 \
   -DEZTRACE_ENABLE_COMPILER_INSTRU=ON \
   -DEZTRACE_ENABLE_MEMORY=ON \
   -DEZTRACE_ENABLE_MPI=ON \
   -DEZTRACE_ENABLE_OMPT=ON \
   -DEZTRACE_ENABLE_OPENMP=ON \
   -DEZTRACE_ENABLE_POSIXIO=ON \
   -DEZTRACE_ENABLE_PTHREAD=ON \
   -DEZTRACE_ENABLE_STARPU=ON \
   -DMPI_LINK_FLAGS="-fopenmp" \
   -DCMAKE_C_COMPILER=clang \
   && make -j 4 \
   && make install)
